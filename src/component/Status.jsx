import './Contact.css';

function Status(props) {
    const infostatus = props.infostatus

    if (infostatus) {
        return <span>Online</span>
    }
    else{
        return <span>Offline</span>
    }
}

export default Status;