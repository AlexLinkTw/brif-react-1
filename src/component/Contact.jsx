import './Contact.css';
import Status from './Status';

function Contact(props) {
    return ( 
        <div className='Contact'>
            <span>{props.firstname}</span>
            <span>{props.lastname}</span>
            <Status infostatus={props.infostatus}/>
        </div>  
    )
  }

  export default Contact;