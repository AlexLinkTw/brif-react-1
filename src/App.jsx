import './App.css'
import Contact from './component/Contact'

function App() {
  return (
    <section>
      <Contact firstname="Hugo" lastname="Sorez" infostatus/>
      <Contact firstname="Lucie"lastname="Peve"  />
      <Contact firstname="Enzo" lastname="Bress" infostatus/>
    </section>
  )
}

export default App
